package com.gitlab.mvysny.slf4j.logger;

import org.junit.jupiter.api.Test;

public class MessageLengthLimitingLoggerTest {
    private final TestLogger logger = new TestLogger();

    @Test
    public void simpleCutter() {
        final MessageLengthLimitingLogger l = new MessageLengthLimitingLogger(10, 1, logger);
        l.println(0, null, "aaa\nbbb\nccc\nddd\neee", null);
        logger.assertMessages("aaa\nbbb", "ccc\nddd", "eee");
    }

    @Test
    public void tooLongMessageNotCutOnNewline() {
        final MessageLengthLimitingLogger l = new MessageLengthLimitingLogger(10, 1, logger);
        l.println(0, null, "aaabbbcccdddeeefff", null);
        logger.assertMessages("aaabbbcccd", "ddeeefff");
    }
}
