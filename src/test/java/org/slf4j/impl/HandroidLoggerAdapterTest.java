package org.slf4j.impl;

import static org.slf4j.impl.HandroidLoggerAdapter.postprocessMessage;

import static org.junit.jupiter.api.Assertions.assertEquals;

import android.util.Log;

import com.gitlab.mvysny.slf4j.AndroidLoggerFactory;
import com.gitlab.mvysny.slf4j.HandroidServiceProvider;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.LoggingEvent;

/**
 * @author Martin Vysny <mavi@vaadin.com>
 */
public class HandroidLoggerAdapterTest {
    @Test
    public void testWhitespaceCharRemoval() {
        assertEquals("Foo     bar", postprocessMessage("Foo\r\2\3\4\5bar"));
    }

    @Test
    public void testRemoveConsecutiveNewlines() {
        assertEquals("\nFoo\nbar\n", postprocessMessage("\n\nFoo\n\n\n\nbar\n"));
    }

    @Test
    public void testSimpleMessage() {
        Log.clear();
        HandroidLoggerAdapter.APP_NAME = "MyApp";

        Logger handroidLogger = new AndroidLoggerFactory().getLogger("Test");

        handroidLogger.trace("Test message");

        assertEquals("MyApp:Test: Test message", Log.output.get(0));
    }

    @Test
    public void testMessageWithThrowable() {
        Log.clear();
        HandroidLoggerAdapter.APP_NAME = "MyApp";

        Logger handroidLogger = new AndroidLoggerFactory().getLogger("Test");

        handroidLogger.trace("Test message", new RuntimeException("Test exception"));

        Assertions.assertTrue(
                Log.output.get(0).startsWith("MyApp:Test: Test message\njava.lang.RuntimeException: Test exception"),

                "The log output must contain the message and the throwable stack trace, " +
                        "but contains '" + Log.output.get(0) + "'"
        );
    }
}
