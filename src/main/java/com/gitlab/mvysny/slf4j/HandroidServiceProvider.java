package com.gitlab.mvysny.slf4j;

import org.slf4j.ILoggerFactory;
import org.slf4j.IMarkerFactory;
import org.slf4j.helpers.BasicMarkerFactory;
import org.slf4j.helpers.NOPMDCAdapter;
import org.slf4j.spi.MDCAdapter;
import org.slf4j.spi.SLF4JServiceProvider;

public class HandroidServiceProvider implements SLF4JServiceProvider {
   private final IMarkerFactory markerFactory = new BasicMarkerFactory();

   @Override
   public ILoggerFactory getLoggerFactory() {
      return new AndroidLoggerFactory();
   }

   @Override
   public IMarkerFactory getMarkerFactory() {
      return markerFactory;
   }

   @Override
   public MDCAdapter getMDCAdapter() {
      return new NOPMDCAdapter();
   }

   @Override
   public String getRequestedApiVersion() {
      return "2.0.x";
   }

   @Override
   public void initialize() {}
}
