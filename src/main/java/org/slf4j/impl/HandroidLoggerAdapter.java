/*
 *  Copyright (c) 2004-2015 Martin Vysny
 *  All rights reserved.
 *
 *  Permission is hereby granted, free  of charge, to any person obtaining
 *  a  copy  of this  software  and  associated  documentation files  (the
 *  "Software"), to  deal in  the Software without  restriction, including
 *  without limitation  the rights to  use, copy, modify,  merge, publish,
 *  distribute,  sublicense, and/or sell  copies of  the Software,  and to
 *  permit persons to whom the Software  is furnished to do so, subject to
 *  the following conditions:
 *
 *  The  above  copyright  notice  and  this permission  notice  shall  be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE  SOFTWARE IS  PROVIDED  "AS  IS", WITHOUT  WARRANTY  OF ANY  KIND,
 *  EXPRESS OR  IMPLIED, INCLUDING  BUT NOT LIMITED  TO THE  WARRANTIES OF
 *  MERCHANTABILITY,    FITNESS    FOR    A   PARTICULAR    PURPOSE    AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE,  ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.slf4j.impl;

import com.gitlab.mvysny.slf4j.AndroidLoggerAdapter;
import com.gitlab.mvysny.slf4j.logger.HandroidLogger;
import com.gitlab.mvysny.slf4j.logger.MessageLengthLimitingLogger;

import org.jetbrains.annotations.NotNull;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * This class handles three issues:
 * <ul>
 *     <li>It allows you to log or suppress DEBUG messages, simply by setting {@link #DEBUG} appropriately. See the field doc for more info.</li>
 *     <li>Logs ALL exceptions, including UnknownHostException and all exceptions caused by this exception. Android filters out any exceptions which were caused by UnknownHostException.
 *     See http://stackoverflow.com/questions/28897239/log-e-does-not-print-the-stack-trace-of-unknownhostexception for details.
 *     </li>
 *     <li>Process the exception message to allow it to log on Android Studio 1.5. See https://code.google.com/p/android/issues/detail?id=194446 and https://code.google.com/p/android/issues/detail?id=194974</li>
 * </ul>
 * @author mvy
 */
public class HandroidLoggerAdapter extends AndroidLoggerAdapter {

    /**
     * True if the debug messages should be logged, false if not. Defaults to false.
     * <p>
     * Good practice is to log debug during development, while not logging debug messages during production.
     * Put this into your android.app.Application's constructor to achieve this:
     * <pre>
     * HandroidLoggerAdapter.DEBUG = BuildConfig.DEBUG;
     * </pre>
     */
    public static boolean DEBUG = false;

    /**
     * Set this to non-null app name (preferably max 10 characters), to have <code>MyApp:MainActivity</code> instead of
     * <code>s*.b*.a*.u*.k*.MainAct*</code> in your code.
     */
    public static String APP_NAME = null;

    /**
     * Set this to 26 or higher, to notify Handroid that Android finally dropped the retarded 23 character limit on logger name.
     * Just set it to <code>Build.VERSION.SDK_INT</code>.
     */
    public static int ANDROID_API_LEVEL = 1;

    /**
     * @deprecated Use {@link #enableLoggingToFirebaseCrashlytics()} instead.
     */
    @Deprecated
    public static void enableLoggingToCrashlytics() {
        enableLoggingToFirebaseCrashlytics();
    }


    /**
     * If called, the log messages are routed to the Firebase Crashlytics library. You must call this AFTER Crashlytics is initialized in your code;
     * see https://github.com/mvysny/slf4j-handroid/issues/5 for more details. Example of proper initialization in your App:
     * <pre><code>
     *     &#64;Override
     *     public void onCreate() {
     *         super.onCreate();
     *         FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
     *         HandroidLoggerAdapter.enableLoggingToFirebaseCrashlytics();
     *         ...
     * </code></pre>
     * <p>
     * Warning: only exception stacktraces logged as WARNING or ERROR are logged into Crashlytics. See {@link #logInternal(int, String, Throwable)}
     * for details.
     * @throws RuntimeException if the Crashlytics library is not on your classpath.
     */
    public static void enableLoggingToFirebaseCrashlytics() {
        logTarget = HandroidLogger.Type.FIREBASE_CRASHLYTICS;
        initSPI();
    }

    /**
     * See {@link #enableLoggingToCrashlytics()}.
     */
    private static HandroidLogger.Type logTarget = HandroidLogger.Type.BASIC;
    /**
     * max length allowed for a log message. If -1 then there is no message limit.
     * Defaults to 4000
     * which is a reasonable default for all Android API levels. See https://gitlab.com/mvysny/slf4j-handroid/-/issues/8
     * for more details.
     */
    private static int maxLength = 4000;
    /**
     * if log message is longer than {@link #maxLength}, attempt to break the log
     * message at a new line between [minLength] and [maxLength] if one exists
     */
    private static int minLength = 3000;

    /**
     * breaks up log messages into multiple logs no longer than maxLength.
     * @param maxLength max length allowed for a log message. If -1 then there is no message limit. Defaults to 4000
     *                  which is a reasonable default for all Android API levels. See https://gitlab.com/mvysny/slf4j-handroid/-/issues/8
     *                  for more details.
     * @param minLength if log message is longer than {@link #maxLength}, attempt to break the log
     *      message at a new line between [minLength] and [maxLength] if one exists. Defaults to 3000.
     */
    public static void setMessageMaxMinLength(int maxLength, int minLength) {
        HandroidLoggerAdapter.maxLength = maxLength;
        HandroidLoggerAdapter.minLength = minLength;
        initSPI();
    }

    static HandroidLogger SPI_LOGGER;

    static {
        initSPI();
    }

    private static void initSPI() {
        HandroidLogger logger = logTarget.createLogger();
        if (maxLength >= 0) {
            logger = new MessageLengthLimitingLogger(maxLength, minLength, logger);
        }
        SPI_LOGGER = logger;
    }

    public HandroidLoggerAdapter(String tag) {
        super(tag);
    }

    /**
     * Handy function to get a loggable stack trace from a Throwable. As opposed to Android logging mechanism, it logs even UnknownHostExceptions.
     * @param tr An exception to log
     */
    @NotNull
    public static String getStackTraceString(@NotNull Throwable tr) {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);
        tr.printStackTrace(pw);
        pw.flush();
        return sw.toString();
    }

    @Override
    protected boolean isLoggable(int priority) {
        return DEBUG || super.isLoggable(priority);
    }

    @Override
    protected void logInternal(int priority, String message, Throwable throwable) {
        if (throwable != null) {
            message += '\n' + getStackTraceString(throwable);
        }
        message = postprocessMessage(message).trim();
        SPI_LOGGER.println(priority, name, message, throwable);
    }

    // visible for testing
    @NotNull
    static String postprocessMessage(@NotNull String message) {
        // we need to do the following, to work around Android Studio 1.5 bugs:
        // 1. remove all characters with code point 0..31 (for example \r) - if those characters
        // are present in the message, the message is not simply logged at all by Android (!!!)
        // see https://code.google.com/p/android/issues/detail?id=194446
        // see https://code.google.com/p/android/issues/detail?id=194974
        // 2. remove two or more consecutive \n: https://code.google.com/p/android/issues/detail?id=195164
        final StringBuilder sb = new StringBuilder(message.length());
        boolean lastCharWasNewLine = false;
        for (int i = 0; i < message.length(); i++) {
            final char c = message.charAt(i);
            if (c != '\n') {
                lastCharWasNewLine = false;
                if (c >= 32) {
                    sb.append(c);
                } else {
                    sb.append(' ');
                }
            } else if (!lastCharWasNewLine) {
                lastCharWasNewLine = true;
                sb.append('\n');
            } // else -> don't append multiple newlines
        }
        return sb.toString();
    }
}
